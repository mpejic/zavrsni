﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZavrsniRad1.Models;

namespace ZavrsniRad1.Controllers
{
    public class PredavanjaController : Controller
    {
        private EvidencijaDbEntities db = new EvidencijaDbEntities();

        // GET: Predavanja
        public ActionResult Index()
        {
            var predavanja = db.Predavanja.Include(p => p.Kolegiji).Include(p => p.Predavači);
            return View(predavanja.ToList().OrderBy(o => o.PredavanjeDatum));
        }

        // GET: Predavanja/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Predavanja predavanja = db.Predavanja.Find(id);
            if (predavanja == null)
            {
                return HttpNotFound();
            }
            return View(predavanja);
        }

        // GET: Predavanja/Create
        public ActionResult Create()
        {
            ViewBag.KolegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij");
            //ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime");
            ViewData["PredavačID"] =
            new SelectList((from s in db.Predavači
                            select new
                            {
                                PredavačID = s.PredavačID,
                                PunoIme = s.Ime + " " + s.Prezime
                            }),
            "PredavačID", "PunoIme", null);
            return View();
        }

        // POST: Predavanja/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PredavanjeID,PredavačID,KolegijID,PredavanjeDatum,PredavanjePočetak,PredavanjeKraj,PredavanjeTekst")] Predavanja predavanja)
        {
            if (ModelState.IsValid)
            {
                db.Predavanja.Add(predavanja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KolegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij", predavanja.KolegijID);
            ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", predavanja.PredavačID);
            return View(predavanja);
        }

        // GET: Predavanja/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Predavanja predavanja = db.Predavanja.Find(id);
            if (predavanja == null)
            {
                return HttpNotFound();
            }
            ViewBag.KolegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij", predavanja.KolegijID);
            //ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", predavanja.PredavačID);
            ViewData["PredavačID"] =
            new SelectList((from s in db.Predavači
                            select new
                            {
                                PredavačID = s.PredavačID,
                                PunoIme = s.Ime + " " + s.Prezime
                            }),
            "PredavačID", "PunoIme", null);
            return View(predavanja);
        }

        // POST: Predavanja/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PredavanjeID,PredavačID,KolegijID,PredavanjeDatum,PredavanjePočetak,PredavanjeKraj,PredavanjeTekst")] Predavanja predavanja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(predavanja).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KolegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij", predavanja.KolegijID);
            ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", predavanja.PredavačID);
            return View(predavanja);
        }

        // GET: Predavanja/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Predavanja predavanja = db.Predavanja.Find(id);
            if (predavanja == null)
            {
                return HttpNotFound();
            }
            return View(predavanja);
        }

        // POST: Predavanja/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Predavanja predavanja = db.Predavanja.Find(id);
            db.Predavanja.Remove(predavanja);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
