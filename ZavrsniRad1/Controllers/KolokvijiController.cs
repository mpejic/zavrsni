﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZavrsniRad1.Models;

namespace ZavrsniRad1.Controllers
{
    public class KolokvijiController : Controller
    {
        private EvidencijaDbEntities db = new EvidencijaDbEntities();

        // GET: Kolokviji
        public ActionResult Index()
        {
            var kolokviji = db.Kolokviji.Include(k => k.Kolegiji).Include(k => k.Predavači);
            return View(kolokviji.ToList().OrderBy(o => o.KolokvijDatum));
        }

        // GET: Kolokviji/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kolokviji kolokviji = db.Kolokviji.Find(id);
            if (kolokviji == null)
            {
                return HttpNotFound();
            }
            return View(kolokviji);
        }

        // GET: Kolokviji/Create
        public ActionResult Create()
        {
            ViewBag.KolegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij");
            //ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime");
            ViewData["PredavačID"] =
            new SelectList((from s in db.Predavači
                            select new
                            {
                                PredavačID = s.PredavačID,
                                PunoIme = s.Ime + " " + s.Prezime
                            }),
            "PredavačID", "PunoIme", null);
            return View();
        }

        // POST: Kolokviji/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KolokvijID,KolegijID,PredavačID,KolokvijDatum,KolokvijStart,KolokvijTekst")] Kolokviji kolokviji)
        {
            if (ModelState.IsValid)
            {
                db.Kolokviji.Add(kolokviji);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KolegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij", kolokviji.KolegijID);
            ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", kolokviji.PredavačID);
            return View(kolokviji);
        }

        // GET: Kolokviji/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kolokviji kolokviji = db.Kolokviji.Find(id);
            if (kolokviji == null)
            {
                return HttpNotFound();
            }
            ViewBag.KolegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij", kolokviji.KolegijID);
            //ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", kolokviji.PredavačID);
            ViewData["PredavačID"] =
            new SelectList((from s in db.Predavači
                            select new
                            {
                                PredavačID = s.PredavačID,
                                PunoIme = s.Ime + " " + s.Prezime
                            }),
            "PredavačID", "PunoIme", null);
            return View(kolokviji);
        }

        // POST: Kolokviji/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KolokvijID,KolegijID,PredavačID,KolokvijDatum,KolokvijStart,KolokvijTekst")] Kolokviji kolokviji)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kolokviji).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KolegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij", kolokviji.KolegijID);
            ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", kolokviji.PredavačID);
            return View(kolokviji);
        }

        // GET: Kolokviji/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kolokviji kolokviji = db.Kolokviji.Find(id);
            if (kolokviji == null)
            {
                return HttpNotFound();
            }
            return View(kolokviji);
        }

        // POST: Kolokviji/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kolokviji kolokviji = db.Kolokviji.Find(id);
            db.Kolokviji.Remove(kolokviji);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
