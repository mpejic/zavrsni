﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZavrsniRad1.Models;

namespace ZavrsniRad1.Controllers
{
    public class IspitiController : Controller
    {
        private EvidencijaDbEntities db = new EvidencijaDbEntities();

        // GET: Ispiti
        public ActionResult Index()
        {
            var ispiti = db.Ispiti.Include(i => i.Kolegiji).Include(i => i.Predavači);
            return View(ispiti.ToList().OrderBy(o => o.IspitDatum));
        }

        // GET: Ispiti/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ispiti ispiti = db.Ispiti.Find(id);
            if (ispiti == null)
            {
                return HttpNotFound();
            }
            return View(ispiti);
        }

        // GET: Ispiti/Create
        public ActionResult Create()
        {
            ViewBag.KoelegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij");
            //ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime");
            ViewData["PredavačID"] =
            new SelectList((from s in db.Predavači
                            select new
                            {
                                PredavačID = s.PredavačID,
                                PunoIme = s.Ime + " " + s.Prezime
                            }),
            "PredavačID", "PunoIme", null);
            return View();
        }

        // POST: Ispiti/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IspitID,KoelegijID,PredavačID,IspitDatum,IspitPočetak,IspitTekst")] Ispiti ispiti)
        {
            if (ModelState.IsValid)
            {
                db.Ispiti.Add(ispiti);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KoelegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij", ispiti.KoelegijID);
            ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", ispiti.PredavačID);
            return View(ispiti);
        }

        // GET: Ispiti/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ispiti ispiti = db.Ispiti.Find(id);
            if (ispiti == null)
            {
                return HttpNotFound();
            }
            ViewBag.KoelegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij", ispiti.KoelegijID);
            //ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", ispiti.PredavačID);
            ViewData["PredavačID"] =
            new SelectList((from s in db.Predavači
                            select new
                            {
                                PredavačID = s.PredavačID,
                                PunoIme = s.Ime + " " + s.Prezime
                            }),
            "PredavačID", "PunoIme", null);
            return View(ispiti);
        }

        // POST: Ispiti/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IspitID,KoelegijID,PredavačID,IspitDatum,IspitPočetak,IspitTekst")] Ispiti ispiti)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ispiti).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KoelegijID = new SelectList(db.Kolegiji, "KolegijID", "Kolegij", ispiti.KoelegijID);
            ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", ispiti.PredavačID);
            return View(ispiti);
        }

        // GET: Ispiti/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ispiti ispiti = db.Ispiti.Find(id);
            if (ispiti == null)
            {
                return HttpNotFound();
            }
            return View(ispiti);
        }

        // POST: Ispiti/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ispiti ispiti = db.Ispiti.Find(id);
            db.Ispiti.Remove(ispiti);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
