﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZavrsniRad1.Models;

namespace ZavrsniRad1.Controllers
{
    public class PredavačiController : Controller
    {
        private EvidencijaDbEntities db = new EvidencijaDbEntities();

        // GET: Predavači
        public ActionResult Index()
        {
            return View(db.Predavači.ToList());
        }

        // GET: Predavači/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Predavači predavači = db.Predavači.Find(id);
            if (predavači == null)
            {
                return HttpNotFound();
            }
            return View(predavači);
        }

        // GET: Predavači/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Predavači/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PredavačID,Ime,Prezime,Titula")] Predavači predavači)
        {
            if (ModelState.IsValid)
            {
                db.Predavači.Add(predavači);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(predavači);
        }

        // GET: Predavači/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Predavači predavači = db.Predavači.Find(id);
            if (predavači == null)
            {
                return HttpNotFound();
            }
            return View(predavači);
        }

        // POST: Predavači/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PredavačID,Ime,Prezime,Titula")] Predavači predavači)
        {
            if (ModelState.IsValid)
            {
                db.Entry(predavači).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(predavači);
        }

        // GET: Predavači/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Predavači predavači = db.Predavači.Find(id);
            if (predavači == null)
            {
                return HttpNotFound();
            }
            return View(predavači);
        }

        // POST: Predavači/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Predavači predavači = db.Predavači.Find(id);  //1
            var kolegiji = db.Kolegiji.Include(c => c.Predavači);
            var predavanja = db.Predavanja.Include(l => l.Kolegiji).Include(l => l.Predavači);
            var ispiti = db.Ispiti.Include(e => e.Kolegiji).Include(e => e.Predavači);
            var kolokviji = db.Kolokviji.Include(c => c.Kolegiji).Include(c => c.Predavači);
            foreach (var c in kolokviji)
            {
                if (c.PredavačID == predavači.PredavačID)
                {
                    db.Kolokviji.Remove(c);
                }
            }
            foreach (var e in ispiti)
            {
                if (e.PredavačID == predavači.PredavačID)
                {
                    db.Ispiti.Remove(e);
                }
            }
            foreach (var l in predavanja)
            {
                if (l.PredavačID == predavači.PredavačID)
                {
                    db.Predavanja.Remove(l);
                }
            }
            foreach (var k in kolegiji)
            {
                if (k.PredavačID == predavači.PredavačID)
                {
                    db.Kolegiji.Remove(k);
                }
            }
            db.Predavači.Remove(predavači); //2
            db.SaveChanges();  //3
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
