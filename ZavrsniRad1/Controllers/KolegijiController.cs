﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZavrsniRad1.Models;

namespace ZavrsniRad1.Controllers
{
    public class KolegijiController : Controller
    {
        private EvidencijaDbEntities db = new EvidencijaDbEntities();

        // GET: Kolegiji
        public ActionResult Index()
        {
            var kolegiji = db.Kolegiji.Include(k => k.Predavači);
            return View(kolegiji.ToList());
        }

        // GET: Kolegiji/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kolegiji kolegiji = db.Kolegiji.Find(id);
            if (kolegiji == null)
            {
                return HttpNotFound();
            }
            return View(kolegiji);
        }

        // GET: Kolegiji/Create
        public ActionResult Create()
        {
            //ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime");
            ViewData["PredavačID"] =
            new SelectList((from s in db.Predavači
                            select new
                            {
                                PredavačID = s.PredavačID,
                                PunoIme = s.Ime + " " + s.Prezime
                            }),
            "PredavačID", "PunoIme", null);
            return View();
        }

        // POST: Kolegiji/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KolegijID,PredavačID,Kolegij,Kratica")] Kolegiji kolegiji)
        {
            if (ModelState.IsValid)
            {
                db.Kolegiji.Add(kolegiji);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", kolegiji.PredavačID);
            return View(kolegiji);
        }

        // GET: Kolegiji/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kolegiji kolegiji = db.Kolegiji.Find(id);
            if (kolegiji == null)
            {
                return HttpNotFound();
            }
            //ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", kolegiji.PredavačID);
            ViewData["PredavačID"] =
            new SelectList((from s in db.Predavači
                            select new
                            {
                                PredavačID = s.PredavačID,
                                PunoIme = s.Ime + " " + s.Prezime
                            }),
            "PredavačID", "PunoIme", null);
            return View(kolegiji);
        }

        // POST: Kolegiji/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KolegijID,PredavačID,Kolegij,Kratica")] Kolegiji kolegiji)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kolegiji).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PredavačID = new SelectList(db.Predavači, "PredavačID", "Ime", kolegiji.PredavačID);
            return View(kolegiji);
        }

        // GET: Kolegiji/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kolegiji kolegiji = db.Kolegiji.Find(id);
            if (kolegiji == null)
            {
                return HttpNotFound();
            }
            return View(kolegiji);
        }

        // POST: Kolegiji/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kolegiji kolegiji = db.Kolegiji.Find(id);
            var predavanja = db.Predavanja.Include(l => l.Kolegiji).Include(l => l.Predavači);
            var ispiti = db.Ispiti.Include(e => e.Kolegiji).Include(e => e.Predavači);
            var kolokviji = db.Kolokviji.Include(c => c.Kolegiji).Include(c => c.Predavači);
            foreach (var c in kolokviji)
            {
                if (c.KolegijID == kolegiji.KolegijID)
                {
                    db.Kolokviji.Remove(c);
                }
            }
            foreach (var e in ispiti)
            {
                if (e.KoelegijID == kolegiji.KolegijID)
                {
                    db.Ispiti.Remove(e);
                }
            }
            foreach (var l in predavanja)
            {
                if (l.KolegijID == kolegiji.KolegijID)
                {
                    db.Predavanja.Remove(l);
                }
            }
            db.Kolegiji.Remove(kolegiji);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
