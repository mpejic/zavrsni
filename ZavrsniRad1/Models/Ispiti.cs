//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZavrsniRad1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public partial class Ispiti
    {

        [DisplayName("Ispit")]
        public int IspitID { get; set; }
        [DisplayName("Kolegij")]
        public int KoelegijID { get; set; }
        [DisplayName("Predavač")]
        public int PredavačID { get; set; }
        [DisplayName("Datum ispita")]
        [DataType(DataType.Date)]
        public System.DateTime IspitDatum { get; set; }
        [DisplayName("Početak ispita")]
        public System.TimeSpan IspitPočetak { get; set; }
        [DisplayName("Napomena")]
        public string IspitTekst { get; set; }
    
        public virtual Kolegiji Kolegiji { get; set; }
        public virtual Predavači Predavači { get; set; }
    }
}
